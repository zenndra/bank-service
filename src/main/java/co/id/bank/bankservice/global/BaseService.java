package co.id.bank.bankservice.global;

import co.id.bank.bankservice.exception.TechnicalException;

public interface BaseService<T1, T2> {
    T2 execute(T1 input) throws Exception;
}
