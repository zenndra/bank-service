package co.id.bank.bankservice.global;

import co.id.bank.bankservice.exception.TechnicalException;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
@Log4j2
public class GlobalExceptionHandler {

    @ExceptionHandler(TechnicalException.class)
    public final ResponseEntity<Object> handleTechnicalException(TechnicalException ex, WebRequest request) {
        log.error("Technical error, {}", ex.getMessage());
        return new ResponseEntity<>(new ErrorResponse(ex.getMessage(), "80000", ex.getTitle()),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<Object> handleException(Exception ex, WebRequest request) {
        log.error("Technical error, {}", ex.getMessage());
        return new ResponseEntity<>(new ErrorResponse(ex.getMessage(), "", "Something went wrong"),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
