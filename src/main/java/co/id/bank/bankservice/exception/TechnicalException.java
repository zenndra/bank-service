package co.id.bank.bankservice.exception;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
public class TechnicalException extends Exception {
    private static final long serialVersionUID = 1L;

    private String title;

    public TechnicalException(String message) {
        super(message);
        this.title = "";
    }

    public TechnicalException(String message, String title) {
        super(message);
        this.title = title;
    }

}
