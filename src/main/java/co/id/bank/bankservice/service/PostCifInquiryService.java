package co.id.bank.bankservice.service;

import co.id.bank.bankservice.adaptor.CifInquiryAdaptor;
import co.id.bank.bankservice.exception.TechnicalException;
import co.id.bank.bankservice.global.BaseService;
import co.id.bank.bankservice.model.request.CifInquiryCoreRequest;
import co.id.bank.bankservice.model.request.PostCifInquiryRequest;
import co.id.bank.bankservice.model.response.CifInquiryCoreResponse;
import co.id.bank.bankservice.model.response.NewsResponse;
import co.id.bank.bankservice.model.response.PostCifInquiryResponse;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class PostCifInquiryService implements BaseService<PostCifInquiryRequest, PostCifInquiryResponse> {

    private CifInquiryAdaptor cifInquiryAdaptor;

    public PostCifInquiryService(CifInquiryAdaptor cifInquiryAdaptor) {
        this.cifInquiryAdaptor = cifInquiryAdaptor;
    }

    @Override
    public PostCifInquiryResponse execute(PostCifInquiryRequest input) throws Exception {
        CifInquiryCoreRequest cifInquiryCoreRequest = CifInquiryCoreRequest.builder()
                .cif(input.getCifNumber())
                .build();
        //CifInquiryCoreResponse cifInquiryCoreResponse = cifInquiryAdaptor.getCifInquiryDetails(cifInquiryCoreRequest);
        CifInquiryCoreResponse cifInquiryCoreResponse = CifInquiryCoreResponse.builder().build();
        NewsResponse newsResponse = cifInquiryAdaptor.getNews();
        return PostCifInquiryResponse.builder()
                .cifNumber(cifInquiryCoreResponse.getCifNumber())
                .mobileNumber(newsResponse.getStatus())
                .address(null)
                .motherMaidenName(cifInquiryCoreResponse.getMotherMaidenName())
                .status(cifInquiryCoreResponse.getCustomerStatus())
                .build();
    }


}
