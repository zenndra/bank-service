package co.id.bank.bankservice.model.response;

import co.id.bank.bankservice.model.dto.Address;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CifInquiryCoreResponse {
    private String cifNumber;
    private String mobileNumber;
    private String motherMaidenName;
    private Address address;
    private String accountNumber;
    private String customerStatus;
}
