package co.id.bank.bankservice.model.response;

import co.id.bank.bankservice.model.dto.Articles;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class NewsResponse {

    private String status;
    private String totalResults;
}
