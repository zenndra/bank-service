package co.id.bank.bankservice.adaptor;

import co.id.bank.bankservice.exception.TechnicalException;
import co.id.bank.bankservice.model.request.CifInquiryCoreRequest;
import co.id.bank.bankservice.model.response.CifInquiryCoreResponse;
import co.id.bank.bankservice.model.response.NewsResponse;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Component
@Log4j2
public class CifInquiryAdaptor {
    private RestTemplate cifRestTemplate;

    @Value("${cif-inquiry-url}")
    private String cifInquiryUrl;

    @Value("${open-news-url}")
    private String newsUrl;

    public CifInquiryAdaptor(RestTemplate cifRestTemplate) {
        this.cifRestTemplate = cifRestTemplate;
    }

    public CifInquiryCoreResponse getCifInquiryDetails(CifInquiryCoreRequest cifInquiryCoreRequest) throws TechnicalException {
        try {
            return cifRestTemplate.exchange(cifInquiryUrl,
                    HttpMethod.POST,
                    new HttpEntity<>(cifInquiryCoreRequest),
                    CifInquiryCoreResponse.class)
                    .getBody();
        } catch (RestClientException ex) {
            log.error("Call url error {}", cifInquiryUrl);
            throw new TechnicalException("Call core error", "Core error");
        }
    }

    public NewsResponse getNews() throws TechnicalException {
        try {
            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(newsUrl);
            return cifRestTemplate.exchange(
                    builder.toUriString(),
                    HttpMethod.GET,
                    null,
                    NewsResponse.class).getBody();
        } catch (RestClientException ex) {
            log.error("Call url error {}", newsUrl);
            throw new TechnicalException("Call news error", "News error");
        }
    }
}
