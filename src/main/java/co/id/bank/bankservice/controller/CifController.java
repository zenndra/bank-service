package co.id.bank.bankservice.controller;

import co.id.bank.bankservice.model.request.PostCifInquiryRequest;
import co.id.bank.bankservice.model.response.PostCifInquiryResponse;
import co.id.bank.bankservice.service.PostCifInquiryService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/api/")
public class CifController {

    private PostCifInquiryService postCifInquiryService;

    public CifController(PostCifInquiryService postCifInquiryService) {
        this.postCifInquiryService = postCifInquiryService;
    }

    @PostMapping(value = "/v1/cif/search")
    public PostCifInquiryResponse cifInquiry(@RequestBody @Valid PostCifInquiryRequest postCifInquiryRequest) throws Exception {
        return postCifInquiryService.execute(postCifInquiryRequest);
    }
}
